<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pier36_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_HOME','http://dev.pier36.com/');
define('WP_SITEURL','http://dev.pier36.com/');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9W@wgnCH.C]8yhlN3`%oFS8ZLyO-7|}:5U[[WzksoZ!B[gyl7&O=r%(z%(54;S!;');
define('SECURE_AUTH_KEY',  'jQK#]UTd%=Ygw=/,pz+bL8J>sA;~(<5Ep@UQ7:Sl1#zv}bh{B98oaTd:fDK#VvM!');
define('LOGGED_IN_KEY',    '`{V%G3)LY^Ur^:r`(s/>58;&?eI[LRFHQ<D?M5/B3}B<<bH)F11033E>Mo$nZ]LS');
define('NONCE_KEY',        'f9U!D<iXCHufl[# S/@%pQc?.J3LDm=uUNp9zXaUFn 4tb6s97#wgp&L>kXgY$ h');
define('AUTH_SALT',        'IsDEYV Re=%=T;g>G!7Z$A:raOsSx: R3wmO<f?2LTIN.aoKZyy5jk$T0H%NCm9R');
define('SECURE_AUTH_SALT', ']!2CXI?kkU9Lz}T1fUG9NH5r0lUj$*e=lIJc]zS)3`f>^_3]<]51jiiA@R{^YJwG');
define('LOGGED_IN_SALT',   'zvp0lM dRy.5N-l}c,A~bPrX&bU,$Gj6vTNjm23qXKGN_!$>VQ0&_@KA_T%c/uRc');
define('NONCE_SALT',       'YC2y[#0BE{d;K$N6fVGl0?KU+I(3R%2/Lzyop~E3Lm/G ku8s2GR2KkcTv}IZeh)');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
