			<!-- footer -->
            <footer>
            	<div class="footer-wrapper">
            		<div class="footer-left">
            			<div class="address">
            				<h3>Location</h3>
            				<p>Pier 36 NYC @ 299 South Street</p>
            				<p>New York, NY</p>
            				<p>bruce@basketballcity.com</p>
            			</div>
            			<div class="connect">
            				<h3>Connect With Us</h3>
            				<ul>
            					<li>
            						<a href="/contact">
            							<img src="<?php echo get_template_directory_uri(); ?>/img/footer-mail.png">
            						</a>
            					</li>
            					<li>
            						<a href="https://twitter.com/pier36nyc?lang=en" target="_blank">
            							<img src="<?php echo get_template_directory_uri(); ?>/img/footer-twitter.png">
            						</a>
            					</li>
            					<li>
            						<a href="https://www.facebook.com/Pier36NYC" target="_blank">
            							<img src="<?php echo get_template_directory_uri(); ?>/img/footer-fb.png">
            						</a>
            					</li>
            					<li>
            						<a href="https://www.instagram.com/pier36nyc/" target="_blank">
            							<img src="<?php echo get_template_directory_uri(); ?>/img/footer-ig.png">
            						</a>
            					</li>
            				</ul>
            			</div>
            		</div>

            		<div class="footer-right">
            			<p class="ape_mono">&#xa9; 2016 Pier36 LLC | powered by <a href="http://emagid.com/">eMagid</a></p>
                <p>
            		</div>
            	</div>



            </footer>
			<!-- /footer -->

		<?php wp_footer(); ?>

		<script type="text/javascript">
		$(document).ready(function(){
		  $('.your-class').slick({
		  	  dots: true,
		  infinite: true,
		  autoplay: true,
		  autoplaySpeed: 3500,
		  arrows: true,
		  centerMode: true,
		  slidesToShow: 3,
		  slidesToScroll: 1,
            responsive: [
                {
                  breakpoint: 600,
                  settings: {
                    arrows: false,
                    slidesToShow: 1,
                    centerMode: false,
                    dots: false
                  }
                }
                ]
		  });
		});
	
            </script>

            <script type="text/javascript">
            $(document).ready(function(){
              $('.mainSlider').slick({
                    dots: false,
              infinite: true,
              autoplay: true,
              autoplaySpeed: 3500,
              arrows: false,
              slidesToShow: 1,
              slidesToScroll: 1
              });
            });
      
            </script>

            <script type="text/javascript">
                

                var $slickFrame = $('.tab-slider-thumbs__list');

var slideWidth = $slickFrame.width() / 3;
var centerPadding = (slideWidth / 4) * 3;

var slickOptions = {
    centerMode: true,
    centerPadding: centerPadding + 'px'
    slidesToShow: 1
};
            </script>

		<!-- analytics -->

	</body>
</html>
