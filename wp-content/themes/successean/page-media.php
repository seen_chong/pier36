<?php get_header(); ?>
            <div class="featured-section">
                  <div class="related-events-wrapper">


                        <div class="related-events media-grid">
                              <div class="events-masonry">

                                    <?php
                                          $args = array(
                                    'post_type' => 'videos'
                                    );
                                          $products = new WP_Query( $args );
                                                if( $products->have_posts() ) {
                                          while( $products->have_posts() ) {
                                          $products->the_post();
                                    ?>
                                    <div class="mason-left">
                                          <div class="mason-t-l">
                                                <?php the_field('link'); ?>
                                                <div class="mason-text">
                                                      <h4><?php the_field('title'); ?></h4>
                                                      <p><?php the_field('description'); ?></p>
                                                </div>
                                          </div>
                                    </div>

                                     <?php
                                          }
                                                }
                                          else {
                                          echo 'No Media Found';
                                          }
                                    ?>


                              </div>
                        </div>                      

                  </div>
                        
            </div>

      </div>
            <!-- featured-section -->

            <div class="events-section">
            	<div class="events-wrapper">
            		<div class="events-header">
            			<h2>Check Out The Events Hosted @ Pier36</h2>

                              <div class="more-event-tabs">
                                    <ul>
                                          <?php
                                                $args = array(
                                          'post_type' => 'private-events'
                                          );
                                                $products = new WP_Query( $args );
                                                      if( $products->have_posts() ) {
                                                while( $products->have_posts() ) {
                                                $products->the_post();
                                          ?>

                                          <li>
                                                <a href="<?php the_permalink(); ?>">
                                                      <?php the_field('event_name'); ?>
                                                </a>
                                          </li>

                                          <?php
                                                }
                                                      }
                                                else {
                                                echo 'No Events Found';
                                                }
                                          ?>
                                    </ul>
                              </div>
                              
                              <div class="featured-header">
                                    <h4>Interested in hosting your next event at Pier 36?</h4>
                              <a href="/contact"><button>Contact Us</button></a>
                              </div>
            		</div>
			</div>
            </div>
            <!-- events-section -->




            <div class="media-section">
                  <div class="media-wrapper">
                        <div class="media-header">
                        <img class="main-logo" src="<?php echo get_template_directory_uri(); ?>/img/icon-twitter.png">
                              <h4>Recent Media</h4>
                        <img class="main-logo" src="<?php echo get_template_directory_uri(); ?>/img/icon-ig.png">
                        </div>

                        <div class="media-boxes">
                              <div class="twitter-slider">
                                    <?php echo do_shortcode( '[ap-twitter-feed-slider controls="true" slide_duration="4000" auto_slide="true"]' ); ?>
                              </div>
                        </div>
                        
                        </div>
                  </div>
            </div>
            <!-- media-section -->

            <div class="map-section">
            	<div class="map-banner">
            		<div class="map-header">
            			<h1>PIER36NYC LOCATION</h1>
            		</div>            		
            	</div>
            	<div class="mapbox">
            	    <img src='https://api.mapbox.com/styles/v1/mapbox/streets-v8/static/-74.003632
,40.705923,13/700x250@2x?access_token=pk.eyJ1Ijoic2VhbmF0ZW1hZ2lkIiwiYSI6ImNpcHJnMWVubjAwajFma20yczliZjBtMDAifQ.pW90nMSDqxpCejZ_am2jDQ' width='100%' height='400'alt='Map of Albany, NY'>
            	</div>
            </div>

        </section>

<?php get_footer(); ?>

<style type="text/css">
      iframe {
            width: 100% !important;
      }
</style>