<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/fonts/font.css">
        
        <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/js/lib/slick/slick.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/js/lib/slick/slick-theme.css" />
		<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<script src="<?php echo get_template_directory_uri(); ?>/js/salvattore.min.js"></script>

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>

        <section class="page">
	            <div class="shortHeader">
	            	<div class="site-wrapper">
		                <div class="left-float">
		                    <a href="tel:2122335050" class="goth_small inv_btn">212 233 5050</a>
		                    <p class="goth_small">299 South Street, New York NY</p>
		                </div>
		                <div class="right-float">
		                    <ul class="social-icons">
		                    	<li>
		                    		<a href="https://www.basketballcity.com/ny" target="_blank">
		                    			<img src="<?php echo get_template_directory_uri(); ?>/img/bball-city-icon.png">
		                    		</a>
		                    	</li>
		                    	<li>
		                    		<a href="https://www.facebook.com/Pier36NYC" target="_blank">
		                    			<img src="<?php echo get_template_directory_uri(); ?>/img/fb-icon.png">
		                    		</a>
		                    	</li>
		                    	<li>
		                    		<a href="https://twitter.com/pier36nyc?lang=en" target="_blank">
		                    			<img src="<?php echo get_template_directory_uri(); ?>/img/twitter-icon.png">
		                    		</a>
		                    	</li>
		                    	<li>
		                    		<a href="https://www.youtube.com/user/Pier36NYC" target="_blank">
		                    			<img src="<?php echo get_template_directory_uri(); ?>/img/youtube-icon.png">
		                    		</a>
		                    	</li>
		                    	<li>
		                    		<a href="https://www.instagram.com/pier36nyc/" target="_blank">
		                    			<img src="<?php echo get_template_directory_uri(); ?>/img/ig-icon.png">
		                    		</a>
		                    	</li>

		                    </ul>
		                </div>
	                </div>
	            </div>
	            <header>
	                <div class="navCenter">

	                	<div class="nav-holder">

		                	<ul class="nav-left">
		                		<?php wp_nav_menu( array( 'theme_location' => 'header-left' ) ); ?>
<!-- 		                		<li><a href="/event">Private Events</a></li>
		                		<li><a href="/venue">Venue Information</a></li> -->
		                	</ul>

		                	<ul class="nav-right">
		                		<?php wp_nav_menu( array( 'theme_location' => 'header-right' ) ); ?>
		                	</ul>
	                	</div>

	                	<div class="left-border"></div>
	                    <a to="/" href="/" style="cursor:pointer" class="navigationSerifImageLogo">
	                        <img class="main-logo" src="<?php echo get_template_directory_uri(); ?>/img/logo.png">
	                    </a>

	                    <div class="right-border"></div>
	                </div>
	            </header>
          