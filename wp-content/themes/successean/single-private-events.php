<?php get_header(); ?>


            <div class="mainContent">
                <div class="postContainer">
                        <div class="eventHeroContent">
                              <div class="date-bg" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/date-bg.png);"><?php the_field('month'); ?><br><span class="day"><?php the_field('day'); ?></span></div>
                              <div class="eventHeroText">
                                    <h3 class="ape_mono"><?php the_field('subtitle'); ?></h3>
                                    <h1 class="ape_mono"><?php the_field('event_name'); ?></h1>
                                    <p><?php the_field('event_details'); ?></p>
                              </div>
                        </div>
                        <div class="seeMore">
                              <p>See Details</p>
                              <img src="<?php echo get_template_directory_uri(); ?>/img/circle-down.png">
                        </div>
                        
                   
                    <div class="media heroBanner">
                    
                    <video poster="<?php the_field('main_banner'); ?>" autoplay="" loop="" muted="" class="fillWidth" style="max-height:auto;width:100%;">
                            <source src="<?php the_field('video_banner'); ?>" type="video/mp4">Your browser does not support the video tag. I suggest you upgrade your browser.
                        </video>
                    </div>
                </div>
            </div>

            <div class="news-box">
            	<div class="news-box-wrapper">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/funky-mail.png">
	            	<p>Sign up to get all the news on upcoming events and new promotions</p>
	            	<input type="email" name="newsletter" placeholder="Enter your Email address" class="newsletterEmail">
				<input type="submit" name="submit" value="Submit" class="updateBtn">
		    </div>
            </div>


            <div class="events-section">
            	<div class="events-wrapper">
            		<div class="events-header">
            			<h2><?php the_field('event_name'); ?></h2>
	            		<p><?php the_field('intro_text'); ?></p>
            		</div>
                        <div class="share-event">

                              <h6>Share the Event</h6>
                              <!-- AddToAny BEGIN -->
                              <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                              <a class="a2a_button_facebook"><img src="<?php echo get_template_directory_uri(); ?>/img/fb-share.png"></a>
                              <a class="a2a_button_twitter"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter-share.png"></a>
                              </div>
                              <script async src="https://static.addtoany.com/menu/page.js"></script>
                              <!-- AddToAny END -->
                              
                        </div>
			</div>
            </div>
            <!-- events-section -->

            <div class="featured-section">
            	<div class="featured-wrapper">

                        <div class="featured-event">
                              <div class="mason-t-r" style="background-image:url(<?php the_field('featured_image'); ?>);">

                                    <div class="image-holder">
                                          <img class="main-logo" src="<?php the_field('featured_image'); ?>">
                                    </div>
                                    <div class="mason-text">
                                          <h4>About</h4>
                                          <p><?php the_field('about'); ?></p>

                                          <h4>Dates</h4>
                                          <p><?php the_field('dates'); ?></p>

                                          <h4>Rooms Used</h4>
                                          <p><?php the_field('rooms_used'); ?></p>
                                    </div>
                              </div> 
                        </div>                      
            		<div class="featured-header">
            			<h4>Interested in hosting your next event at Pier 36?</h4>
            			<a href="/contact"><button>Contact Us</button></a>
            		</div>

            	</div>
            		
            </div>

            <div class="featured-section">
                  <div class="related-events-wrapper">
                        <div class="related-events-header ">
                              <h4>- Related Events -</h4>
                        </div>

                        <div class="related-events">
                              <div class="events-masonry">

                                    <?php
                                          $args = array(
                                    'post_type' => 'private-events',
                                    'orderby'=>'rand',
                                    'posts_per_page' => 2 
                                    );
                                          $products = new WP_Query( $args );
                                                if( $products->have_posts() ) {
                                          while( $products->have_posts() ) {
                                          $products->the_post();
                                    ?>
                                    <div class="mason-left">
                                          <div class="mason-t-l" style="background-image:url(<?php the_field('featured_image'); ?>">
                                                <a href="<?php the_permalink(); ?>">
                                                      <img class="main-logo" src="<?php the_field('featured_image'); ?>">
                                                </a>
                                                <div class="mason-text">
                                                      <a href="<?php the_permalink(); ?>">
                                                            <h4><?php the_field('event_name'); ?></h4>
                                                      </a>
                                                      <p><?php the_field('about'); ?></p>
                                                </div>
                                          </div>
                                    </div>

                                     <?php
                                          }
                                                }
                                          else {
                                          echo 'No Events Found';
                                          }
                                    ?>


                              </div>
                        </div>                      

                  </div>
                        
            </div>

      </div>
            <!-- featured-section -->

            <div class="media-section">
                  <div class="media-wrapper">
                        <div class="media-header">
                        <img class="main-logo" src="<?php echo get_template_directory_uri(); ?>/img/icon-twitter.png">
                              <h4>Recent Media</h4>
                        <img class="main-logo" src="<?php echo get_template_directory_uri(); ?>/img/icon-ig.png">
                        </div>

                        <div class="media-boxes">
                              <div class="twitter-slider">
                                    <?php echo do_shortcode( '[ap-twitter-feed-slider controls="true" slide_duration="4000" auto_slide="true"]' ); ?>
                              </div>

                        </div>
                        </div>
                  </div>
            </div>
            <!-- media-section -->

            <div class="map-section">
            	<div class="map-banner">
            		<div class="map-header">
            			<h1>PIER36NYC LOCATION</h1>
            		</div>            		
            	</div>
                  <div class="mapbox">
                      <img src='https://api.mapbox.com/styles/v1/mapbox/streets-v8/static/-74.003632
,40.705923,13/700x250@2x?access_token=pk.eyJ1Ijoic2VhbmF0ZW1hZ2lkIiwiYSI6ImNpcHJnMWVubjAwajFma20yczliZjBtMDAifQ.pW90nMSDqxpCejZ_am2jDQ' width='100%' height='400'alt='Map of Albany, NY'>
                  </div>
            </div>

        </section>

<?php get_footer(); ?>
