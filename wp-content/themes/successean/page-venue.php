<?php get_header(); ?>


            <div class="mainContent">
                <div class="postContainer">
                        <div class="eventHeroContent">
                              <div class="eventHeroText">
                                    <h3 class="ape_mono">Overview</h3>
                                    <h1 class="ape_mono">Venue Information</h1>
                                    <p>massive rooms | 15,000 person capacity | 125,000 total sq feet</p>
                              </div>
                        </div>
                        
                   
                    <div class="media heroBanner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/AVB-Pier94-2014_1229_231539-2614_DVS.jpg);"></div>
                </div>
            </div>

            <div class="goto-box">
                  <div class="goto-box-wrapper">
                        <div class="goto-tabs">
                              <a href="#venue-specs"><button>Venue Specs</button></a>
                              <a href="#prev-events"><button>Previous Events</button></a>
                              <a href="#images"><button>Images</button></a>
                              <a href="#recent-media"><button>Recent Media</button></a>
                        </div>
		      </div>
            </div>


            <div class="specs-section" id="venue-specs">
            	<div class="specs-wrapper">
            		<div class="specs-header">
            			<h2>Pier36 Specifications</h2>
	            		<p>Basketball City & Pier 36 host <strong>700,000 visitors per year & 1,500 top corporations</strong> with their year-round programming.</p>
            		</div>
                  </div>
                  <div class="specs-content">
                        <div class="funky-moon">
                              <img src="<?php echo get_template_directory_uri(); ?>/img/funky-moon.png">
                        </div>
                        <div class="specs-image">
                              <img src="<?php echo get_template_directory_uri(); ?>/img/pier-site1.jpg">
                        </div>
                        <div class="specs-right">
                              <div class="specs-sheet">
                                    <h5>The Breakdown</h5>
                                    <ul>
                                          <li>"State of the art" Sports and Entertainment Facility</li>
                                          <li>Located at Pier 36 on South Street (off the FDR Drive)</li>
                                          <li>70,000 sq ft facility</li>
                                          <li>55,000 sq ft parking lot</li>
                                          <li>15,000 sq ft deck overlooking East River with views of downtown Manhattan, South Street Seaport, Manhattan/Brooklyn Bridges and Statue of Liberty.</li>
                                          <li>150 mbs of fiber (with Wi-Fi)</li>
                                    </ul>
                              </div>
                        </div>
                  </div>

                        <div class="specs-bullets">
                              <div class="specs-wrapper">
                                    <div class="specs-list-left">
                                          <ul>
                                                <li>Basketball Programs (youth and adult)</li>
                                                <li>Catered Events</li>
                                                <li>Concerts</li>
                                                <li>Corporate Events</li>
                                                <li>Expos</li>
                                                <li>Technology Conferences</li>
                                                <li>Fashion and Trade Shows</li>
                                          </ul>
                                          
                                    </div>
                                    <div class="specs-list-right">
                                          <ul>
                                                <li>Investment Presentations</li>
                                                <li>Non-Profit Fundraisers</li>
                                                <li>Product Launches</li>
                                                <li>Upfronts / NewsFronts</li>
                                                <li>Team Building Events</li>
                                                <li>Boat Docking for ferries and pleasure boats</li>
                                          </ul>
                                          
                                    </div>
                              </div>
                        </div>

                        <div class="featured-header">
                              <h4>Interested in hosting your next event at Pier 36?</h4>
                              <a href=""><button>Contact Us</button></a>
                        </div>
			
            </div>
            <!-- events-section -->

            <div class="more-events-section" id="prev-events">
            	<div class="more-events-wrapper">

                        <div class="more-events-header">
                              <h2>Some of the Events</h2>
                        </div>    
            		<!-- <div class="more-events-nav">
                              <h4>&mdash; Previous &ensp; Upcoming &mdash;</h4>
                        </div> -->

            	

                              <div class="more-event-tabs">
                                    <ul>
                                          <?php
                                                $args = array(
                                          'post_type' => 'private-events'
                                          );
                                                $products = new WP_Query( $args );
                                                      if( $products->have_posts() ) {
                                                while( $products->have_posts() ) {
                                                $products->the_post();
                                          ?>

                                          <li>
                                                <a href="<?php the_permalink(); ?>">
                                                      <?php the_field('event_name'); ?>
                                                </a>
                                          </li>

                                          <?php
                                                }
                                                      }
                                                else {
                                                echo 'No Events Found';
                                                }
                                          ?>
                                    </ul>
                              </div>

                        <div class="lightbox-gallery" id="images">
                              <?php echo do_shortcode("[huge_it_gallery id='1']"); ?>
                        </div>


                  </div>
	                 <!-- more-events-wrapper -->
            </div>
            <!-- more-events-section -->

      </div>
            

            <div class="media-section" id="recent-media">
                  <div class="media-wrapper">
                        <div class="media-header">
                        <img class="main-logo" src="<?php echo get_template_directory_uri(); ?>/img/icon-twitter.png">
                              <h4>Recent Media</h4>
                        <img class="main-logo" src="<?php echo get_template_directory_uri(); ?>/img/icon-ig.png">
                        </div>

                        <div class="media-boxes">

                              <div class="twitter-slider">
                                    <?php echo do_shortcode( '[ap-twitter-feed-slider controls="true" slide_duration="4000" auto_slide="true"]' ); ?>
                              </div>

                        </div>

                        </div>
                  </div>
            </div>
            <!-- media-section -->

            

            <div class="map-section">
            	<div class="map-banner">
            		<div class="map-header">
            			<h1>PIER36NYC LOCATION</h1>
            		</div>            		
            	</div>
            	<div class="mapbox">
            	    <img src='https://api.mapbox.com/styles/v1/mapbox/streets-v8/static/-74.003632
,40.705923,13/700x250@2x?access_token=pk.eyJ1Ijoic2VhbmF0ZW1hZ2lkIiwiYSI6ImNpcHJnMWVubjAwajFma20yczliZjBtMDAifQ.pW90nMSDqxpCejZ_am2jDQ' width='100%' height='400'alt='Map of Albany, NY'>
            	</div>
            </div>

        </section>

<?php get_footer(); ?>

<style>
.element_1 .title-block_1 {
    display: none !important;
}
</style>

<script type="text/javascript">
$('a[href^="#"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1000);
    }
});
</script>
