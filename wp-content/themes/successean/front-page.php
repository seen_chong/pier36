<?php get_header(); ?>


            <div class="mainContent mainSlider">

            <?php
	  			$args = array(
	    		'post_type' => 'private-events'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

                <div class="postContainer">
                    <div class="eventHeroContent">
                          <div class="eventHeroText">
                                <h3 class="ape_mono"><?php the_field('subtitle'); ?></h3>
                                <h1 class="ape_mono"><?php the_field('event_name'); ?></h1>
                                <p><?php the_field('event_details'); ?></p>

                          </div>
                          <div class="event-lead">
                          	<a href="<?php the_permalink(); ?>">
                          		<img src="<?php echo get_template_directory_uri(); ?>/img/funky-arrow.png">
                          	</a>
                          </div>
                    </div>
                   
                    <div class="media heroBanner" style="background-image:url(<?php the_field('main_banner'); ?>);"></div>
                </div>

	        <?php
				}
					}
				else {
				echo 'No Events Found';
				}
			?>

			</div>

      

            <div class="news-box">
            	<div class="news-box-wrapper">
            		<img src="<?php echo get_template_directory_uri(); ?>/img/funky-mail.png">
	            	<p>Sign up to get all the news on upcoming events & new promotions</p>
	            	<input type="email" name="newsletter" placeholder="Enter your Email address" class="newsletterEmail">
					<input type="submit" name="submit" value="Submit" class="updateBtn">
				</div>
            </div>


            <div class="events-section">
            	<div class="events-wrapper">
            		<div class="events-header">
            			<h2>Featured Events</h2>
	            		<p>Some of the biggest brands host the most epic events @ Pier36.</p>
            		</div>

            		<div class="events-masonry">

            			<div class="mason-left">

            			<?php
                              $args = array(
                        'post_type' => 'private-events',
                        'orderby'=>'rand',
                        'posts_per_page' => 2 
                        );
                              $products = new WP_Query( $args );
                                    if( $products->have_posts() ) {
                              while( $products->have_posts() ) {
                              $products->the_post();
                        ?>

	            			<div class="mason-t-l" style="background-image:url(<?php the_field('featured_image'); ?>);">
	            				<a href="<?php the_permalink(); ?>">
	            					<img class="main-logo" src="<?php the_field('featured_image'); ?>">
	            				</a>
	            				<div class="mason-text">
	            					<a href="<?php the_permalink(); ?>">
	            						<h4><?php the_field('event_name'); ?></h4>
	            					</a>
	            					<p><?php the_field('about'); ?></p>
	            				</div>
	            			</div>

	            		<?php
                              }
                                    }
                              else {
                              echo 'No Events Found';
                              }
                        ?>

            			</div>





            			<div class="mason-right">

            			<?php
                              $args = array(
                        'post_type' => 'private-events',
                        'orderby'=>'rand',
                        'posts_per_page' => 5 
                        );
                              $products = new WP_Query( $args );
                                    if( $products->have_posts() ) {
                              while( $products->have_posts() ) {
                              $products->the_post();
                        ?>

	            			<div class="mason-t-r" style="background-image:url(<?php the_field('featured_image'); ?>);">

	            				<div class="image-holder">
	            					<a href="<?php the_permalink(); ?>">
	            						<img class="main-logo" src="<?php the_field('featured_image'); ?>">
	            					</a>
	            				</div>
	            				<div class="mason-text">
	            					<a href="<?php the_permalink(); ?>">
		            					<h4><?php the_field('event_name'); ?></h4>
		            				</a>
	            					<p><?php the_field('intro_text'); ?></p>
	            				</div>
	            			</div>

	            		<?php
                              }
                                    }
                              else {
                              echo 'No Events Found';
                              }
                        ?>

<!-- 	            			<div class="mason-t-r" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/event-nye.jpg);">

	            				<div class="image-holder">
	            					<img class="main-logo" src="<?php echo get_template_directory_uri(); ?>/img/event-nye.jpg">
	            				</div>
	            				<div class="mason-text">
	            					<h4>New Years Eve Concert 2013</h4>
	            					<p>Armin Van Buuren at Pier 36 NYC – New Years 2013</p>
	            				</div>
	            			</div>

	            			<div class="mason-t-r" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/event-beautycon.jpg);">

	            				<div class="image-holder">
	            					<img class="main-logo" src="<?php echo get_template_directory_uri(); ?>/img/event-beautycon.jpg">
	            				</div>
	            				<div class="mason-text">
	            					<h4>Beautycon 2014</h4>
	            					<p>BeautyCon is a fashion and beauty summit for the internet’s most influential icons and their ever-growing number of fans.</p>
	            				</div>
	            			</div>

	            			<div class="mason-t-r" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/event-eats.jpg);">

	            				<div class="image-holder">
	            					<img class="main-logo" src="<?php echo get_template_directory_uri(); ?>/img/event-eats.jpg">
	            				</div>
	            				<div class="mason-text">
	            					<h4>Village Voice Choice Ets 2015</h4>
	            					<p>Featuring over 70+ handpicked restaurants from all five boroughs, </p>
	            				</div>
	            			</div> -->
            			</div>


            		</div>
            		<!-- events-masonry -->
	            	
				</div>
            </div>
            <!-- events-section -->

            <div class="specs-section">
            	<div class="specs-wrapper">
            		<div class="specs-header">
            			<h4>Interested in hosting your next event at Pier 36?</h4>
            			<a href="/contact"><button>Contact Us</button></a>
            		</div>

            		<div class="spec-boxes">
            			<div class="spec-title">
            				<h2>Specs</h2>
            			</div>
            			<div class="first-box box">
            				<h3>Logistics</h3>
            				<ul>
            					<li>Easy Access for commercial and large vehicles</li>
            					<li>Conveniently located off the FDR Drive</li>
            					<li>55,000 Square foot lot for loading/unloading</li>
            				</ul>
            			</div>
            			<div class="second-box box">
            				<h3>Rigging</h3>
            				<ul>
            					<li>Certified Riggers</li>
            					<li>Large number of rigging points</li>
            					<li>Flexible Space</li>
            				</ul>
            			</div>
            			<div class="third-box box">
            				<h3>AV Capabilities</h3>
            				<ul>
            					<li>LED Walls</li>
            					<li>Video Servers</li>
            					<li>Concert Audio Line Array System</li>
            					<li>Lobby Lighting Packages</li>
            				</ul>
            			</div>

            		</div>
            		
            	</div>


            </div>
            <!-- specs-section -->

            <div class="media-section">
            	<div class="media-wrapper">
            		<div class="media-header">
                        <img class="main-logo" src="<?php echo get_template_directory_uri(); ?>/img/icon-twitter.png">
            			<h4>Recent Media</h4>
                        <img class="main-logo" src="<?php echo get_template_directory_uri(); ?>/img/icon-ig.png">
            		</div>

            		<div class="media-boxes">

            			<div class="twitter-slider">
                        	<?php echo do_shortcode( '[ap-twitter-feed-slider controls="true" slide_duration="4000" auto_slide="true"]' ); ?>
                      	</div>

            		</div>
            		</div>
            	</div>
            </div>
            <!-- media-section -->

            <div class="map-section">
            	<div class="map-banner">
            		<div class="map-header">
            			<h1>PIER36NYC LOCATION</h1>
            		</div>            		
            	</div>
                  <div class="mapbox">
                      <img src='https://api.mapbox.com/styles/v1/mapbox/streets-v8/static/-74.003632
,40.705923,13/700x250@2x?access_token=pk.eyJ1Ijoic2VhbmF0ZW1hZ2lkIiwiYSI6ImNpcHJnMWVubjAwajFma20yczliZjBtMDAifQ.pW90nMSDqxpCejZ_am2jDQ' width='100%' height='400'alt='Map of Albany, NY'>
                  </div>
            </div>

        </section>

<?php get_footer(); ?>


