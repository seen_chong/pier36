<?php get_header(); ?>


            <div class="mainContent">
                <div class="postContainer">
                        <div class="eventHeroContent">
                              <div class="eventHeroText">
                                    <h1 class="ape_mono">Contact Us</h1>
                                    <p>email | call | or drop us a line ==></p>
                              </div>
                        </div>                        
                   
                    <div class="media heroBanner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/banner1.jpg);"></div>
                </div>
            </div>


            <div class="contact-section">
            	<div class="contact-wrapper">
            		<div class="contact-header">
	            		<p>Don't Hesitate to get in touch</p>
                              <ul>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/icon-phone.png">212 233 5050</li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/icon-envelope.png">info@pier36nyc.com</li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/icon-pin.png">Pier 36 @ 299 South Street</li>
                              </ul>
            		</div>
                        <div class="contact-form">
                              <div class="contact-title">
                                    <h2>Send A Message</h2>
                              </div>
                              
                              <div class="inqSwitch">
                                    <h6 class="event-title active"><a id="event-click">Event Inquiries</a></h6>
                                    <h6 class="press-title"><a id="press-click">Press Inquiries</a></h6>
                              </div>
                              <div class="event-form">
                                    <?php echo do_shortcode("[contact-form-7 id='4' title='Event Inquiries']"); ?>
                              </div>
                               <div class="press-form" style="display:none;">
                                    <?php echo do_shortcode("[contact-form-7 id='54' title='Press Inquiries']"); ?>
                              </div>
                              

                        </div>
			</div>
            </div>
            <!-- events-section -->

            <div class="direct-section">

                  <div class="direct-wrapper">
                        <div class="direct-trans">
                              <h3>Directions By Subway</h3>
                              <h5>F Train to East Broadway</h5>
                              <p>Take Rutgers St./Madison St. exit from station - Exit Walk east on Rutgers Street towards the water. Turn left on South Street heading north. Once you approach the Dept of Sanitation (on your right), Pier 36 is adjacent on the north side of Sanitation.</p>
                        </div>

                        <div class="direct-trans">
                              <h3>Directions By Car</h3>

                              <h5>From FDR Drive - Southbound:</h5>
                              <p>FDR Drive South to Exit 3, towards South St/Manhattan Bridge. Stay straight to go onto FDR Drive. FDR Drive becomes South Street. Pier 36 is located at the intersection of South Street and Montgomery Street</p>

                              <h5>From FDR Drive - Northbound: </h5>
                              <p>South Street north (under the FDR highway), past the South Street Seaport (do not go on the FDR Drive). Continue north on South Street (under the Manhattan Bridge, Pathmark on your left), until you see the Department of Sanitation on your right. At the next intersection (South Street and Montgomery Street), turn right at the light for Pier 36. (the entrance for the FDR north will be directly in front of you at the light).</p>

                              <h5>From New Jersey Turnpike:</h5>
                              <p>Proceed to Exit 14C (Holland Tunnel) - Go through the Holland Tunnel and Take EXIT 5 on the left toward Canal St East.Turn slight right onto Laight St...Turn slight right onto Canal St... Turn right onto Bowery. Bowery becomes Chatham Sq. Turn left onto E Broadway...Turn right onto Samuel Dickstein Plz... Turn slight left onto Montgomery St. Turn right onto South St. and end at Pier 36, located at the intersection of South Street and Montgomery Street</p>

                              <h5>From the George Washington Bridge: </h5>
                              <p>After leaving GW Bridge, take Harlem River Drive exit, Exit 2, towards FDR Drive Proceed onto the Harlem River Drive which connects/merges into FDR Drive S. Take FDR Drive South to Exit 3, towards South St/ Manhattan Bridge. At the light at the bottom of the ramp – turn left and proceed under the highway - Pier 36 is directly in front of you. Pier 36 is located at the intersection of South Street and Montgomery Street</p>

                              <h5>From Lincoln Tunnel: </h5>
                              <p>Proceed through tunnel. Stay straight to go onto Dyer Avenue. Turn Left at West 34th Street. Merge onto FDR South to Battery Park. Take Exit 3, towards South St/Manhattan Bridge. Stay straight to go onto FDR Drive. FDR Drive becomes South Street. Pier 36 is located at the intersection of South Street and Montgomery Street</p>

                              <h5>From Queens & Long Island:</h5>
                              <p>Take the L.I.E. to the Midtown Tunnel/Manhattan. After Midtown Tunnel, Turn left on East 34th Street. Merge onto FDR South to Battery Park. Take Exit 3, towards South St/Manhattan Bridge. Stay straight to go onto FDR Drive. FDR Drive becomes South Street. Pier 36 is located at the intersection of South Street and Montgomery Street</p>


                        </div>
                  </div>
            </div>


            <div class="map-section">
            	<div class="map-banner">
            		<div class="map-header">
            			<h1>PIER36NYC LOCATION</h1>
            		</div>            		
            	</div>
            	<div class="mapbox">
            	    <img src='https://api.mapbox.com/styles/v1/mapbox/streets-v8/static/-74.003632
,40.705923,13/700x250@2x?access_token=pk.eyJ1Ijoic2VhbmF0ZW1hZ2lkIiwiYSI6ImNpcHJnMWVubjAwajFma20yczliZjBtMDAifQ.pW90nMSDqxpCejZ_am2jDQ' width='100%' height='400'alt='Map of Albany, NY'>
            	</div>
            </div>

        </section>

<?php get_footer(); ?>

<script>
      $(document).ready(function() {
            $('a#press-click').click(function() {
                  $('.press-form').show();
                  $('.event-form').hide();
                  $('h6.event-title').removeClass('active');
                  $('h6.press-title').addClass('active');
            });
            $('a#event-click').click(function() {
                  $('.event-form').show();
                  $('.press-form').hide();
                  $('h6.press-title').removeClass('active');
                  $('h6.event-title').addClass('active');
            });
      });
</script>