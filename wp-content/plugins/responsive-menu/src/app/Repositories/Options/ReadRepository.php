<?php

namespace ResponsiveMenu\Repositories\Options;

interface ReadRepository {
  public function all();
}
